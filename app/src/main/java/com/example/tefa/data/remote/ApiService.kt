package com.example.tefa.data.remote

import com.example.tefa.data.remote.response.UserList
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("api/users")
    suspend fun getUserList(
        @Query("page") page: Int,
    ): UserList.Response
}